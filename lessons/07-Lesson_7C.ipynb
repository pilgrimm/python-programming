{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Namespaces\n",
    " In Python, _namespaces_ are what store the names of all variables, functions, classes, modules, etc. used in the program. A namespaces kind of behaves like a big dictionary that maps the name to the thing named.\n",
    "\n",
    "The two major namespaces are the **global** namespace and the **local** namespace. The global namespace is accessible from everywhere in the program. The local namespace will change depending on the current scope - whether you are in a function, loop, class, module, etc. Besides local and global namespaces, each module has its own namespace."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Global namespace\n",
    "\n",
    "`dir()` with no arguments actually shows you the names in the global namespace."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's copy awesome.py file from practice folder\n",
    "!cp ../practice/awesome.py awesome.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from awesome import *\n",
    "from awesome import cool as coolgroup\n",
    "\n",
    "def main():\n",
    "    a = Awesome(\"Everything\")\n",
    "    print(a)\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    main()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dir()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another way to see this is with the `globals()` function, which returns a dictionary of not only the names but also their values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sorted(globals().keys())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dir() == sorted (globals(). keys())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "globals()['Awesome']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# what's wrong?\n",
    "globals()['awesome']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "globals()['cool']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "globals()['coolgroup']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " ## Local namespace\n",
    "\n",
    "The local namespace can be accessed using `locals()`, which behaves just like `globals()`. Right now, the local namespace and the global namespace are the same. We're at the top level of our code, not inside a function or anything else."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "globals() == locals()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's take a look at it in a different scope.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "import awesome\n",
    "def altered_cool():\n",
    "    # still there?\n",
    "    print(awesome.Awesome('Artisanal vinegar'))\n",
    "    print(coolgroup('the intelligentsia'))\n",
    "    cool = 'hipster'\n",
    "    lumberjack = True\n",
    "    print(sorted(locals().keys()))\n",
    "    print(locals()['cool'] )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "altered_cool()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "'lumberjack' in globals()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "globals()['cool']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "globals() == locals()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Packages\n",
    "\n",
    "What if you want to organize your code into multiple modules? Since a module is a file, the natural thing to do is to gather all your related modules in to a single directory or folder. And, indeed, a Python **package** is just that: a directory that contains modules, a special `__init__.py` file, and sometimes more packages or other helper files."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "sound/                    Top-level package\n",
    "    _init_.py             Initialize the sound package\n",
    "     formats/             Subpackage for file format conversions\n",
    "            __init__.py\n",
    "            wavread.py\n",
    "            wavwrite.py\n",
    "            aiffread.py\n",
    "            aiffwrite.py\n",
    "            ...\n",
    "     effects/             Subpackage for sound effects\n",
    "            __init__.py\n",
    "            echo.py\n",
    "            surround.py\n",
    "            reverse.py\n",
    "     filters/             Subpackage for filters\n",
    "            __init__.py\n",
    "            equalizer.py\n",
    "            vocoder.py\n",
    "            karaoke.py\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can access submodules by chaining them together with dot notation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "sys.path.append('../practice') #add ../practice/sound to path\n",
    "import sound.effects.reverse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The other methods of importing work as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "from sound.filters import karaoke"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `__init__.py`\n",
    "\n",
    "What is this special `__init__.py` file?\n",
    "\n",
    " * Its presence is required to tell Python that the directory is a package\n",
    " * It can be empty, as long as it's there\n",
    " * It's typically used to initialize the package (as the name implies)\n",
    "\n",
    "`__init__.py` can contain any code, but it’s best to keep it short and focused on just what’s needed to initialize and manage the package.For example:\n",
    "\n",
    "* setting the `__all__` variable to tell Python what modules to include when someone runs `from package import *`\n",
    "* automatically import some of the submodules so that when someone runs `import package` then they can run `package.function` rather than `package.submodule.function`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "## Module Namespaces\n",
    "\n",
    "Finally, each module also has its own _module_ namespace. You can inspect them using the module's special `__dict__` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import awesome\n",
    "sorted(awesome. __dict__.keys())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# guess what?\n",
    "dir(awesome) == sorted(awesome._dict_.keys())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# can also print this to get the memory Location\n",
    "awesome.__dict__['cool']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# didn't we just see that here?\n",
    "from awesome import cool as coolgroup\n",
    "globals()['coolgroup']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import awesome as awe\n",
    "dir(awe)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "awe.__dict__['cool']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "awe == awesome"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "id(awe) == id(awesome)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "## Modifying module namespaces\n",
    "\n",
    "You can add to module namespaces on the fly. Keep in mind, though, that this will only last until the program exits, and the actual module file will be unchanged."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "def more_awesome():\n",
    "    return \"They're awesome!\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "awe.exclaim = more_awesome"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "awe.exclaim()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'exclaim' in dir(awe)\n",
    "'exclaim' in dir(awesome)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "notebook_metadata_filter": "-all",
   "text_representation": {
    "extension": ".md",
    "format_name": "markdown"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
