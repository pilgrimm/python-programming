{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "# I see you like Python, so I put Python in your Python\n",
    "\n",
    "We've seen how to write scripts; now we want to reuse the good parts. We've already used the `import` command, which lets us piggyback on the work of others-either through Python's extensive standard library or through additional, separately-installed packages. It can also be used to proactively leverage the long tail of our own personal production. In this lesson, we cover, in much greater depth, the mechanics and principles of writing and distributing modules and packages. Suppose you have a script named `my_funcs.py` in your current directory. Then the following works just fine:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create my_funcs.py file using linux command\n",
    "!touch my_funcs.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "import my_funcs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "import my_funcs as m"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "import importlib\n",
    "importlib.reload(m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "# fix this ...\n",
    "contents = '''\n",
    "def test():\n",
    "        return \"ok\";\n",
    "'''\n",
    "\n",
    "with open('my_funcs.py', 'w') as f:\n",
    "    f.write(contents)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "# can you fix the code above to make this work?\n",
    "from my_funcs import test"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "# BE CAREFUL\n",
    "from my_funcs import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "If you change the source file `my_funcs.py` in between `import` commands, you will have different versions of the functions imported. So what's going on?\n",
    "\n",
    "## Namespaces\n",
    "\n",
    "When you `import` a **module** (what we used to call merely a script), Python executes it as if from the command line, then places variables and\n",
    "functions inside a **namespace** defined by the script name (or using the optional `as` keyword). When you `from <module> import <name>`, the variables are imported into your current namespace. Think of a namespace as a super-variable that contains references to lots of other variables, or as a super-class that can contain data, functions, and classes.\n",
    "After import, a module is dynamic like any Python object; for example, the `reload` function takes a module as an argument, and you can add data and methods to the module after you've imported it (but they won’t persist beyond the lifetime of your script or session)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "import my_funcs as m"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "def silly_func(x):\n",
    "    return \"Silly {}!\".format(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "m.silly_func = silly_func"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "m.silly_func(\"Mark\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "In contrast, the `from <module> import <function>` command adds the function to the current namespace.\n",
    "\n",
    "## Preventing Excess Output: The Magic of `__main__`\n",
    "\n",
    "Suppose you have a script that does something awesome, called `awesome.py`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "contents = '''\n",
    "class Awesome(object):\n",
    "    def __init__(self, awesome_thing):\n",
    "        self.thing = awesome_thing\n",
    "    def __str__(self):\n",
    "        return \"{0.thing} is AWESOME.\".format(self)\n",
    "\n",
    "a = Awesome(\"BASE Jumping\")\n",
    "print(a)\n",
    "'''\n",
    "\n",
    "with open('awesome.py', 'w') as f:\n",
    "    f.write(contents)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "!python awesome.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "import awesome"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "# what is this printing?\n",
    "awesome.a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "print(awesome.a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "You don't want that print statement to execute every time you import it. Of equal importance, `awesome.a` is probably extraneous within an import. Let's fix it to get rid of those when you import the module, but keep them when you execute the script."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "contents = '''\n",
    "class Awesome(object):\n",
    "    def __init__ (self , awesome_thing):\n",
    "        self.thing = awesome_thing\n",
    "    def __str__ (self ):\n",
    "        return \"{0.thing} is AWESOME.\".format(self)\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    a = Awesome(\"BASE Jumping\")\n",
    "        print(a)\n",
    "'''\n",
    "\n",
    "with open('awesome.py', 'w') as f:\n",
    "    f.write(contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " We can do even better. There are some situations, e.g. profiling or testing, where we would want to import the module, then look at what would happen if we run it as a script. To enable that, move the main functionality in to a function called `main()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "contents = '''\n",
    "class Awesome(object):\n",
    "    def __init__ (self, awesome_thing):\n",
    "        self.thing = awesome_thing\n",
    "    def __str__ (self):\n",
    "        return \"{0.thing} is AWESOME.\".format(self)\n",
    "\n",
    "def main():\n",
    "    a = Awesome(\"BASE Jumping\")\n",
    "    print(a)\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    main()\n",
    "'''\n",
    "\n",
    "with open('awesome.py', 'w') as f:\n",
    "    f.write(contents)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "notebook_metadata_filter": "-all",
   "text_representation": {
    "extension": ".md",
    "format_name": "markdown"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
